package principal;

import java.io.File;
import java.io.FileOutputStream;

import javax.swing.JOptionPane;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;

public class Principal {

	public static void main(String[] args) {
		// Por defecto es A4
		Document documento = new Document();
		try {
			// Obtenemos una instancia de un objeto PDFWriter
			PdfWriter
					.getInstance(documento, new FileOutputStream("d:/pba.pdf"));
			documento.open();
 
			// otenemos el ancho del codumento
			float documentWidth = documento.getPageSize().getWidth();
			// otenemos el largo del codumento
			float documentHeight = documento.getPageSize().getHeight();
			
			//Colocamos la ruta de la carpeta donde se encuentran las imagenes
			String dirr = "C:/Users/ononitram/Pictures/fondo/programador/";
			//Creamos una archivo que contenta la carpeta donde se encuentran los archivos
			File dir = new File(dirr);
			//obtenemos los archivos que estan dentro de  la carpeta
			String[] ficheros = dir.list();
			
			if (ficheros == null) {//si es null quiere decir que no hay archivos
				System.out
						.println("No hay ficheros en el directorio especificado");
			}
			else {//si entra aqui e sporque encontro archivos dentro de la carpeta
				//ficheros.length obtenemos el tamaño del arreglo (numero de archivos encontrados)
				for (int x = 0; x < ficheros.length; x++) {
					//generamos la instancia de la imagen
					Image imagen = Image.getInstance(dir + "/" + ficheros[x]);
					//Le damos una posicion ala imagen
					imagen.setAbsolutePosition(-15, 0);
					//le damos un ancho tamaño ala imagen (EL tamaño del documento)
					imagen.scaleAbsoluteWidth(documentWidth);
					//le damos un largo tamaño ala imagen (EL tamaño del documento)
					imagen.scaleAbsoluteHeight(documentHeight);
					//agregamos la imagen al documento
					documento.add(imagen);
					//creamos una nueva pagina en el documento
					documento.newPage();
					// Se imprime el nombre del archivo
					// System.out.println(ficheros[x]);
				}
			}			
			//cerramos el documento
			documento.close();
			//solo para verificar imprimimos las medidas del documento
			/*
			System.out.println("Archivo generado Width=>" + documentWidth
					+ " Heigh=>" + documentHeight);
					*/

		} catch (DocumentException ex) {
			ex.printStackTrace();
			// Atrapamos excepciones concernientes al documentoo.
		} catch (java.io.IOException ex) {
			ex.printStackTrace();
			// Atrapamos excepciones concernientes al I/O.
		}
	}
}
